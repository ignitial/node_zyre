/*
 * Copyright (c) 2014 V. Andritoiu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <v8.h>
#include <node.h>
#include <node_version.h>
#include <node_buffer.h>
#include <zyre.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdexcept>
#include <set>
#include "nan.h"

using namespace v8;
using namespace node;

namespace zyre {

  static zctx_t *ctx;

  class Node;

  class Poller : public ObjectWrap {

    friend class Node;

    public:
      static void Initialize(v8::Handle<v8::Object> target);
      virtual ~Poller();

    private:
      static NAN_METHOD(New);
      Poller(void *reader);
      
      static NAN_METHOD(Destroy);
      static NAN_METHOD(Add);
      static NAN_METHOD(Wait);

      void *poller_;
  };

  class Node : public ObjectWrap {
    public:
      static void Initialize(v8::Handle<v8::Object> target);
      virtual ~Node();
      static void reception_task(void *args, zctx_t *ctx, void *pipe);
      void CallbackOnMessage(zmsg_t* msg);
      void *socket() { return socket_; };
      void *node() { return node_; };
      const char *uuid() { return uuid_; };

    private:
      static NAN_METHOD(New);
      Node(const String::Utf8Value &name);

      static NAN_GETTER(GetUUID);
      static NAN_SETTER(SetVerbose);
      static NAN_GETTER(GetVerbose);
      static NAN_SETTER(SetInterval);
      static NAN_GETTER(GetInterval);
      static NAN_SETTER(SetPort);
      static NAN_GETTER(GetPort);
 
      static NAN_METHOD(Destroy);

      static NAN_METHOD(SetHeader);

      static NAN_METHOD(Start);
      static NAN_METHOD(Stop);
      static NAN_METHOD(Join);
      static NAN_METHOD(Leave);

      static NAN_METHOD(Shouts);
      static NAN_METHOD(Whispers);

      static NAN_METHOD(Shout);
      static NAN_METHOD(Whisper);

      static NAN_METHOD(Dump);
      static NAN_METHOD(Test);

      void *node_;
      void *socket_;
      size_t interval_;
      int port_;
      bool verbose_;
      const char * uuid_;
  };


  Persistent<String> callback_symbol;

  static void
  Initialize(Handle<Object> target);

  /*
   * Helpers for dealing with ØMQ errors.
   */

  static inline const char*
  ErrorMessage() {
    return zmq_strerror(zmq_errno());
  }

  static inline Local<Value>
  ExceptionFromError() {
    return Exception::Error(String::New(ErrorMessage()));
  }


  /*
   * Node methods.
   */

  void
  Node::Initialize(v8::Handle<v8::Object> target) {
    NanScope();

    Local<FunctionTemplate> t = FunctionTemplate::New(New);
    t->InstanceTemplate()->SetInternalFieldCount(1);
    t->InstanceTemplate()->SetAccessor(
      String::NewSymbol("uuid"), Node::GetUUID);
    t->InstanceTemplate()->SetAccessor(
      String::NewSymbol("port"), GetPort, SetPort);
    t->InstanceTemplate()->SetAccessor(
      String::NewSymbol("verbose"), GetVerbose, SetVerbose);
    t->InstanceTemplate()->SetAccessor(
      String::NewSymbol("interval"), GetInterval, SetInterval);

    NODE_SET_PROTOTYPE_METHOD(t, "setHeader", SetHeader);
    NODE_SET_PROTOTYPE_METHOD(t, "destroy", Destroy);
    NODE_SET_PROTOTYPE_METHOD(t, "start", Start);
    NODE_SET_PROTOTYPE_METHOD(t, "stop", Stop);
    NODE_SET_PROTOTYPE_METHOD(t, "join", Join);
    NODE_SET_PROTOTYPE_METHOD(t, "leave", Leave);
    NODE_SET_PROTOTYPE_METHOD(t, "shouts", Shouts);
    NODE_SET_PROTOTYPE_METHOD(t, "whispers", Whispers);
    NODE_SET_PROTOTYPE_METHOD(t, "test", Test);
    NODE_SET_PROTOTYPE_METHOD(t, "dump", Dump);

    NanAssignPersistent(callback_symbol, NanNew<String>("message"));

    target->Set(String::NewSymbol("NodeBinding"), t->GetFunction());
  }

  Node::~Node() {
    zyre_destroy((zyre_t**) &node_);
  }

  NAN_METHOD(Node::New) {
    NanScope();
    assert(args.IsConstructCall());
    
    if (args.Length() != 1)
      return NanThrowError("Must pass a node name");

    if (!args[0]->IsString())
       return NanThrowTypeError("Name must be a string"); 
    String::Utf8Value name(args[0].As<String>());

    Node *node = new Node(name);
    node->Wrap(args.This());
    NanReturnValue(args.This());
  }

  Node::Node(const String::Utf8Value &name) : ObjectWrap() {
    node_ = zyre_new();
    zyre_set_name((zyre_t*) node_, *name);

    if (!node_) throw std::runtime_error(ErrorMessage());
    verbose_ = false;
    interval_ = 1000;
    port_ = 5670;

    zyre_set_port((zyre_t*) node_, port_);
    uuid_ = zyre_uuid((zyre_t*) node_);
    socket_ = zyre_socket((zyre_t*) node_);
    if (!ctx) {
      ctx = zctx_new();
    }
  }

  void Node::reception_task(void *args, zctx_t *ctx, void *pipe) {   
    Node *node = (Node *) args;
    zmq_pollitem_t items [] = {
        { pipe, 0, ZMQ_POLLIN, 0 },
        { zyre_socket ((zyre_t*) node->node()), 0, ZMQ_POLLIN, 0 }
    };

    while (true) {
        if (zmq_poll (items, 2, -1) == -1)
            break;              //  Interrupted
            
        //  Activity on my pipe
        if (items [0].revents & ZMQ_POLLIN) {
            zmsg_t *msg = zmsg_recv (pipe);
            zmsg_dump (msg);
        }
        //  Activity on my node handle
        if (items [1].revents & ZMQ_POLLIN) {
            zmsg_t *msg = zyre_recv ((zyre_t*) args);
            zmsg_dump (msg);
            node->CallbackOnMessage(msg);
            /*char *command = zmsg_popstr (msg);
            if (streq (command, "SHOUT")) {
              zmsg_dump (msg);
              //  Discard sender and group name
              free (zmsg_popstr (msg));
              free (zmsg_popstr (msg));
              char *message = zmsg_popstr (msg);
              printf ("%s", message);
              free (message);
            }
            free (command);*/
            zmsg_destroy (&msg);
        }
    }
  }

  void
  Node::CallbackOnMessage(zmsg_t* msg) {
    if (msg) {
      NanScope();

      Local<Value> callback_v = NanObjectWrapHandle(this)->Get(NanNew(callback_symbol));
      if (!callback_v->IsFunction()) {
        return;
      }

      TryCatch try_catch;

      size_t size = zmsg_size(msg);
      Handle<Value> argv[size+1];
      argv[0] = NanNew<Integer>(size);

      for (unsigned i = 0; i < size; i++) {
        char *submsg = zmsg_popstr (msg);
        // an error argument:
        argv[i+1] = NanNew<String>(submsg);
        free(submsg);
      }

      NanMakeCallback(NanObjectWrapHandle(this), callback_v.As<Function>(), size+1, argv);

      if (try_catch.HasCaught()) {
        FatalException(try_catch);
      }
    }
  }


  NAN_GETTER(Node::GetUUID) {
    NanScope();
    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    NanReturnValue(String::New(node->uuid_));
  }

  NAN_SETTER(Node::SetPort) {
    NanScope();
    if (!value->IsInt32()) {
      NanThrowTypeError("Port must be an integer");
    }

    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    node->port_ = value->Int32Value();
    zyre_set_port((zyre_t*) node->node_, node->port_);
  }

  NAN_GETTER(Node::GetPort) {
    NanScope();

    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    NanReturnValue(Integer::New(node->port_));
  }

  NAN_SETTER(Node::SetInterval) {
    NanScope();
    if (!value->IsInt32()) {
      NanThrowTypeError("Interval must be an integer");
    }

    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    node->interval_ = value->Int32Value();
    zyre_set_interval((zyre_t*)node->node_, node->interval_);
  }

  NAN_GETTER(Node::GetInterval) {
    NanScope();
    
    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    NanReturnValue(Integer::New(node->interval_));
  }

  NAN_SETTER(Node::SetVerbose) {
    NanScope();
    if (!value->IsBoolean()) {
      NanThrowTypeError("Verbose must be a bool");
    }

    if (!value->BooleanValue()) {
      NanThrowTypeError("Verbose cannot be unset");
    }

    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    node->verbose_ = value->BooleanValue();
    zyre_set_verbose((zyre_t*)node->node_);
  }

  NAN_GETTER(Node::GetVerbose) {
    NanScope();
    
    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    NanReturnValue(Boolean::New(node->verbose_));
  }

  NAN_METHOD(Node::Destroy) {
    NanScope();

    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    zyre_destroy((zyre_t**) &node->node_);
    
    NanReturnUndefined();
  }

  NAN_METHOD(Node::Start) {
    NanScope();

    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    int res = zyre_start((zyre_t*) node->node_);

    zthread_fork(ctx, Node::reception_task, node);

    NanReturnValue(Integer::New(res));
  }

  NAN_METHOD(Node::Stop) {
    NanScope();

    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    zyre_stop((zyre_t*) node->node_);

    NanReturnUndefined();
  }

  NAN_METHOD(Node::SetHeader) {
    NanScope();
    if (args.Length() != 2)
      return NanThrowError("Must pass a name and a header");

    if (!args[0]->IsString())
       return NanThrowTypeError("Name must be a string"); 
    String::Utf8Value name(args[0].As<String>());

    if (!args[1]->IsString())
       return NanThrowTypeError("Header must be a string"); 
    String::Utf8Value header(args[1].As<String>());

    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    zyre_set_header((zyre_t*) node->node_, *name, *header);

    NanReturnUndefined();
  }

  NAN_METHOD(Node::Join) {
    NanScope();
    if (args.Length() != 1)
      return NanThrowError("Must pass a group name");
    if (!args[0]->IsString())
       return NanThrowTypeError("Group name must be a string");
    String::Utf8Value name(args[0].As<String>());

    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    zyre_join((zyre_t*) node->node_, *name);
    zclock_sleep (250);
    
    NanReturnUndefined();
  }

  NAN_METHOD(Node::Leave) {
    NanScope();
    if (args.Length() != 1)
      return NanThrowError("Must pass a group name");
    if (!args[0]->IsString())
       return NanThrowTypeError("Group name must be a string");
    String::Utf8Value name(args[0].As<String>());

    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    zyre_leave((zyre_t*) node->node_, *name);

    NanReturnUndefined();
  }

  NAN_METHOD(Node::Shouts) {
    NanScope();
    if (args.Length() != 2)
      return NanThrowError("Must pass a group and a message");

    if (!args[0]->IsString())
       return NanThrowTypeError("Group must be a string");
    String::Utf8Value group(args[0].As<String>());

    if (!args[1]->IsString())
       return NanThrowTypeError("Message must be a string");
    String::Utf8Value msg(args[0].As<String>());

    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    zmsg_t *rawmsg = zmsg_new();
    zmsg_pushstr(rawmsg, node->uuid());
    zmsg_pushstr(rawmsg, *msg);
    zyre_shout((zyre_t*) node->node_, *group, &rawmsg);
    zmsg_destroy(&rawmsg);

    NanReturnUndefined();
  }

  NAN_METHOD(Node::Whispers) {
    NanScope();
    if (args.Length() != 2)
      return NanThrowError("Must pass a peer and a message");

    if (!args[0]->IsString())
       return NanThrowTypeError("Peer must be a string");
    String::Utf8Value peer(args[0].As<String>());

    if (!args[1]->IsString())
       return NanThrowTypeError("Message must be a string");
    String::Utf8Value msg(args[1].As<String>());

    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    zyre_whispers((zyre_t*) node->node_, *peer, const_cast<char *>("%s"), *msg);

    NanReturnUndefined();
  }

  NAN_METHOD(Node::Test) {
    NanScope();
    if (args.Length() != 1)
      return NanThrowError("Must pass verbose value");

    if (!args[0]->IsBoolean())
       return NanThrowTypeError("Verbose must be a boolean");

    zyre_test(args[0]->BooleanValue());

    NanReturnUndefined();
  }

  NAN_METHOD(Node::Dump) {
    NanScope();

    Node* node = ObjectWrap::Unwrap<Node>(args.Holder());
    zyre_dump((zyre_t*) node->node_);

    NanReturnUndefined();
  }

  /*
   * Poller methods.
   */
  void
  Poller::Initialize(v8::Handle<v8::Object> target) {
    NanScope();

    Local<FunctionTemplate> t = FunctionTemplate::New(New);
    t->InstanceTemplate()->SetInternalFieldCount(1); 

    NODE_SET_PROTOTYPE_METHOD(t, "destroy", Destroy);
    NODE_SET_PROTOTYPE_METHOD(t, "add", Add);
    NODE_SET_PROTOTYPE_METHOD(t, "wait", Wait);

    target->Set(String::NewSymbol("PollerBinding"), t->GetFunction());
  }

  Poller::~Poller() {
    zpoller_destroy((zpoller_t**) &poller_);
  }

  NAN_METHOD(Poller::New) {
    NanScope();
    assert(args.IsConstructCall());
   
    if (args.Length() != 1)
      return NanThrowError("Must pass a node to the constructor");

    assert(args[0]->ToObject()->InternalFieldCount() > 0);
    Node* node = ObjectWrap::Unwrap<Node>(args[0]->ToObject());

    Poller *poller = new Poller(node);

    poller->Wrap(args.This());
    NanReturnValue(args.This());
  }

  Poller::Poller(void *reader) : ObjectWrap() {
    poller_ = zpoller_new(reader);
    if (!poller_) throw std::runtime_error(ErrorMessage());
  }

  NAN_METHOD(Poller::Add) {
    NanScope();
    if (args.Length() != 1)
      return NanThrowError("Must pass a reader node");

    if (!args[0]->IsObject())
       return NanThrowTypeError("Node must be an object");
    
    Node* node = ObjectWrap::Unwrap<Node>(args[0]->ToObject());
    assert(node);

    Poller* poller = ObjectWrap::Unwrap<Poller>(args.Holder());
    zpoller_add((zpoller_t*) poller->poller_, node->socket());

    NanReturnUndefined();
  }

  NAN_METHOD(Poller::Wait) {
    NanScope();
    if (args.Length() != 1)
      return NanThrowError("Must pass a timeout value");

    if (!args[0]->IsInt32())
       return NanThrowTypeError("Timemout must be an integer");

    Poller* poller = ObjectWrap::Unwrap<Poller>(args.Holder());
    void *sock = zpoller_wait((zpoller_t*) poller->poller_, args[0]->Int32Value());
    if (sock) {
      NanReturnValue(Boolean::New(true));
    }

    NanReturnUndefined();
  }

  NAN_METHOD(Poller::Destroy) {
    NanScope();

    Poller* poller = ObjectWrap::Unwrap<Poller>(args.Holder());
    zpoller_destroy((zpoller_t**) &poller->poller_);

    NanReturnUndefined();
  }


  /*
   * Module methods
   */

  static NAN_METHOD(ZmqVersion) {
    NanScope();
    int major, minor, patch;
    zmq_version(&major, &minor, &patch);

    char version_info[16];
    snprintf(version_info, 16, "%d.%d.%d", major, minor, patch);

    NanReturnValue(String::New(version_info));
  }

  static void
  Initialize(Handle<Object> target) {
    NanScope();

    NODE_SET_METHOD(target, "zmqVersion", ZmqVersion);

    NODE_DEFINE_CONSTANT(target, ZMQ_POLLIN);
    NODE_DEFINE_CONSTANT(target, ZMQ_POLLOUT);
    NODE_DEFINE_CONSTANT(target, ZMQ_POLLERR);

    Poller::Initialize(target);
    Node::Initialize(target);   
  }
} // namespace zyre


// module

extern "C" void
init(Handle<Object> target) {
  zyre::Initialize(target);
}

NODE_MODULE(zyre, init)
