# zyre

  [Zyre](http://zguide.zeromq.org/hx:chapter8) bindings for node.js.
  [Zyre on GitHub](https://github.com/zeromq/zyre)

## WARNING: work in progress

## Installation

[Install zyre package](https://github.com/zeromq/zyre) first.

    $ npm install zyre

## Example

znode.js:

```js
var zyre = require('../'),
  util = require('util');

var znode = zyre.node();

console.log(znode.uuid);
console.log(znode.port);
console.log(znode.interval);
console.log(znode.verbose);

znode.verbose = true;
znode.setHeader('uuid', znode.uuid);
znode.start();
znode.join('CHAT');
znode.dump();

znode.on('message', function(data) {
  console.log(data);
});

var i = 0;
setInterval(function(){
	var msg = util.format("%s: I shouted %d time(s). Don't you hear me ?", znode.uuid, ++i);
  console.log(msg);
  znode.shouts('CHAT', msg);
}, 500);

process.on('SIGINT', function() {
	znode.leave('CHAT');
	znode.stop();
  process.exit();
});
```

