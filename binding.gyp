{
  'targets': [
    {
      'target_name': 'zyre',
      'sources': [ 'binding.cc' ],
      'include_dirs' : [
        "<!(node -e \"require('nan')\")"
      ],
      'conditions': [
        ['OS=="win"', {
          'include_dirs': ['windows/include'],
          'link_settings': {
            'libraries': [
              'Delayimp.lib',
            ],
            'conditions': [
              ['target_arch=="ia32"', {
                'libraries': [
                  '<(PRODUCT_DIR)/../../windows/lib/x86/libzyre-v100-mt-3_2_2.lib',
                ]
              },{
                'libraries': [
                  '<(PRODUCT_DIR)/../../windows/lib/x64/libzyre-v100-mt-3_2_2.lib',
                ]
              }]
            ],
          },
          'msvs_settings': {
            'VCLinkerTool': {
              'DelayLoadDLLs': ['libzyre-v100-mt-3_2_2.dll']
            }
          },
        }, {
          'libraries': ['-lzyre'],
          'cflags!': ['-fno-exceptions'],
          'cflags_cc!': ['-fno-exceptions'],
        }],
        ['OS=="mac" or OS=="solaris"', {
          'xcode_settings': {
            'GCC_ENABLE_CPP_EXCEPTIONS': 'YES'
          },
          # add macports include & lib dirs, homebrew include & lib dirs
          'include_dirs': [
            '<!@(pkg-config libzyre --cflags-only-I | sed s/-I//g)',
            '/opt/local/include',
            '/usr/local/include',
          ],
          'libraries': [
            '<!@(pkg-config libzyre --libs)',
            '-L/opt/local/lib',
            '-L/usr/local/lib',
          ]
        }],
        ['OS=="openbsd" or OS=="freebsd"', {
          'include_dirs': [
            '<!@(pkg-config libzyre --cflags-only-I | sed s/-I//g)',
            '/usr/local/include',
          ],
          'libraries': [
            '<!@(pkg-config libzyre --libs)',
            '-L/usr/local/lib',
          ]
        }],
        ['OS=="linux"', {
          'cflags': [
            '<!(pkg-config libzyre --cflags 2>/dev/null || echo "")',
          ],
          'libraries': [
            '<!(pkg-config libzyre --libs 2>/dev/null || echo "")',
          ],
        }],
      ]
    }
  ]
}
