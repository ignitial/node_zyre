var zyre = require('../'),
  util = require('util');

var znode = zyre.node(Math.random().toString(36).slice(2));
//znode.test(true);

console.log(znode.uuid);
console.log(znode.port);
console.log(znode.interval);
console.log(znode.verbose);

znode.verbose = true;
console.log("node start: ", znode.start());
znode.join('CHAT');
znode.dump();

znode.on('message', function() {
  console.log(arguments);
});

var i = 0;
setInterval(function(){
	var msg = util.format("%s: I shouted %d time(s). Don't you hear me ?", znode.uuid, ++i);
  console.log(msg);
  znode.shouts('CHAT', msg);
}, 500);

process.on('SIGINT', function() {
	znode.leave('CHAT');
	znode.stop();
  process.exit();
});